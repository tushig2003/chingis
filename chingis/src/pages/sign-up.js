import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useFirebase } from '../Hooks/firebase';
// import { AuthContext } from '../providers/auth-user-provider'
import { FormInput, Button } from '../components/index'

export const SignUp = () => {
    // const { user } = useContext(AuthContext)
    const [state, setState] = useState({ email: '', password: '', password2: '', username: '' });
    const history = useHistory();
    const [error, setError] = useState('');
    const { firebase, auth, firestore } = useFirebase();

    // const snapshot = await firestore.collection('Events').get()
    // snapshot.docs.map(doc => console.log(doc.data()))

    const handleChangeUsername = (e) => setState({ ...state, username: e.target.value })
    const handleChangeEmail = (e) => setState({ ...state, email: e.target.value });
    const handleChangePassword = (e) => setState({ ...state, password: e.target.value });
    const handleChangePassword2 = (e) => setState({ ...state, password2: e.target.value });

    // if (user) {
    //     history.push('/')
    // }

    const logout = () => {
        auth.signOut();
    }

    const facebook = () => {
        var provider = new firebase.auth.FacebookAuthProvider();

        auth.signInWithPopup(provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            console.log(token);
            console.log(user);
            console.log(user.emailVerified)

            // history.push('/')
        }).catch(function (error) {
            var errorMessage = error.message;
            alert(errorMessage)
        });
    }

    const google = () => {
        var provider = new firebase.auth.GoogleAuthProvider();


        auth.signInWithPopup(provider).then((result) => {
            var token = result.credential.accessToken;
            var user = result.user;

            console.log(token);
            console.log(user);

            console.log(user.emailVerified)
            if (user.emailVerified === true) {
                // history.push('/')
            } else {
                history.push('/Verify')
            }
        }).catch(function (error) {
            var errorMessage = error.message;

            console.log(errorMessage)
        });
    }



    const signUp = async () => {
        if (!(state.email && state.password && state.password2)) {
            setError('Please enter all the required information');
            return;
        }
        if (state.password !== state.password2) {
            setError('Passwords dont match!');
            return;
        }
        let cred = await auth.createUserWithEmailAndPassword(state.email, state.password)
            .catch((error) => {
                alert(error.message)
            })

        await firestore.collection('users').doc(cred.user.uid).set({
            username: state.username,
            createdAt: firebase.firestore.FieldValue.serverTimestamp(),
        });

        history.push('/Verify')
    }

    return (
        <div>
            <div>
                <p>Нэвтрэх</p>
                <FormInput label='Хэрэглэгчийн нэр' type='text' placeholder='Username' value={state.username} onChange={handleChangeUsername} />
                <FormInput label='Цахим хаяг' placeholder='name@mail.com' type='email' value={state.email} onChange={handleChangeEmail} />
                <FormInput label='Нууц үг' placeholder='Password' type='password' value={state.password} onChange={handleChangePassword} />
                <FormInput label='Нууц үгээ давтна уу?' placeholder='Password' type='password' value={state.password2} onChange={handleChangePassword2} />

                {error && <div>{error}</div>}
                <Button onClick={signUp}>Бүртгүүлэх</Button>
                <Button onClick={logout}>Logout</Button>

            </div>
            <div>
                <p>FB</p>
                <button onClick={facebook}>FB</button>

                <p>Google</p>
                <button onClick={google}>Google</button>
            </div>
        </div>
    )
}
