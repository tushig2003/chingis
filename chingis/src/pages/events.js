import React from 'react'
import { useDoc } from '../Hooks/firebase'

export const ListEvents = () => {
    // const { data } = useCol('Events');
    const { data } = useDoc('Events/2gAz6fR');

    console.log(data)

    return (
        <div className='flex-col'>
            <div className='flex-col br-black-3 h-350 w-300'>
                <div className='flex items-center justify-center h-175 w-294'>"PHOTO"</div>
                <div className="br-black-1 h-1 w-294"></div>
                
                <div className='flex-center'>
                    {
                        data && (
                            <div>
                                <h3> {data.name }</h3>
                                <h4> {data.desc }</h4>
                            </div>
                        )
                    }
                </div>
            </div>
        </div>
    )
}