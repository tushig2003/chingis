import React, { useState, useContext, useRef } from 'react';
import { Button, FormInput } from '../components';
import { useCol } from '../Hooks/firebase';
import {AuthContext} from '../providers/auth-user-provider'
import { useFirebase } from '../Hooks/firebase';

export const AddEvent = () => {

    const { user } = useContext(AuthContext)
    const [eventName, setEventName] = useState('');
    const [desc, setDesc] = useState('');
    const [category, setCategory] = useState('');
    const { auth, firebase} = useFirebase();
    const inputFile = useRef(null);

    let c = [];
    let uid;

    if(user != null) {
        uid = user.uid
    }
    
    const {createRecord} = useCol(`/users/${uid}/createdEvents/`);
    const {data} = useCol(`/Categories`);

    // console.log(data);

    const handleChangeEventName = (e) => setEventName(e.target.value);
    const handleChangeDesc = (e) => setDesc(e.target.value);
    const handleChangeCategory = (e) => setCategory(e.target.value);

    const randomStringAndNumber = () => {
        let result = '';
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for ( let i = 0; i < 7; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    const Add = () => {
        let id = randomStringAndNumber();
        console.log(user);
        auth.onAuthStateChanged((u) => {
            if (u) {
                console.log(u);
            } else {
                console.log('bhgui')
            }
        });

        if(category) {
            let categories = category.split(' ');
            let a;

            let b = data.map((e) => {
                return categories.filter((f) => {
                    // console.log(f, '1========2', e.category)
                    return e.category === f;
                });
            });

            a = b.filter((e) => {
                return e.length !== 0;
            })

            for(var i = 0; i < a.length; i++) {
                c = c.concat(a[i]);
            }              

        }


        console.log(id);

        createRecord(id, {name: eventName, desc: desc, id: id, categories: c, vote: 0});
    
    }

    const onButtonClick = () => {
        // `current` points to the mounted file input element
        inputFile.current.click();
        var input = document.getElementById('file')
        input.addEventListener('change', onFileChange)




        function onFileChange() {
            let file = input.files[0];
            document.getElementById('img').src = URL.createObjectURL(file);
            console.log(document.getElementById('img').src)
            console.log(file)
            var storageRef = firebase.storage().ref().child(`profileImages/${user.uid}-profileImage.jpg`);;
            storageRef.put(file)
            .then((snapshot) => {    
                console.log('Done.');
            })
        }
        
    };

    const console2 = () => {
        console.log(inputFile);
    }


    return(
        <div>
            {/* zurag avna */}
            <input type='file' id='file' ref={inputFile} style={{display: 'none'}}/>
            <button onClick={onButtonClick}>Open file upload window</button>
            <Button onClick={console2}>console</Button>

            <img id='img' className='w-100 h-100' alt='img'></img>


            {/* </form> */}

            <FormInput placeholder='Event Name' value={eventName} onChange={handleChangeEventName} />

            <FormInput placeholder='Description' value={desc} onChange={handleChangeDesc} />

            {/* add category */}

            <FormInput placeholder='Categories' value={category} onChange={handleChangeCategory} />

            

            {/* <FormInput placeholder='Description' value={desc} onChange={handleChangeDesc} /> */}

            <Button onClick={Add}>AddEvent</Button>
        </div>
    )
}