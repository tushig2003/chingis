import React from 'react';
import { SignUp } from './pages/sign-up';
import { SignIn } from './pages/sign-in';
import { HomeDefault } from './pages/home-default';
import { AddEvent } from './pages/add-event'
import { Verify } from './pages/verify';
import { ListEvents } from './pages';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import { AuthUserProvider } from './providers/auth-user-provider';
import './style/grid.scss'
import './style/grid-align.scss'
import './style/main.scss';

const App = () => {
    return (
        <AuthUserProvider>
            <Router>
                <Switch>
                    <Route path="/listevents">
                        <ListEvents />
                    </Route>
                    <Route path="/login">
                        <SignIn />
                    </Route>
                    <Route path="/register">
                        <SignUp />
                    </Route>
                    <Route path="/addEvent">
                        <AddEvent />
                    </Route>
                    <Route path="/verify">
                        <Verify />
                    </Route>
                    <Route path="/" exact={true}>
                        <HomeDefault />
                    </Route>
                    <Route path="/event/1">

                    </Route>
                </Switch>
            </Router>
        </AuthUserProvider>
    )
}

export default App
